import datetime as dt
import time

import pandas

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import *

import sys

sys.path.append('../../whatsapp_objects')

from .generic_class import FatherWhatsappMsg


class ImgMsg(FatherWhatsappMsg):
    def __init__(self, selenium_obj, msg_date: dt.date = None):
        super(ImgMsg, self).__init__(selenium_obj, msg_date)
        self.msg_type = 'image'

    def get_img_url(self):
        img_objs = self._obj.find_elements(by=By.CLASS_NAME, value='jciay5ix')
        for i in img_objs:
            src = i.get_attribute('src')
            if src is not None:
                break
        return src  # might be None if image is not loaded
