import datetime as dt

from selenium.webdriver.common.by import By


class FatherWhatsappMsg:
    def __init__(self, selenium_obj, msg_date: dt.date = None):
        self._obj = selenium_obj
        self._msg_date = msg_date
        self.msg_type = None

    def get_msg_sender(self):
        try:
            # todo male it faster! maybe with xpaths
            msg_sub = self._obj.find_element(By.CLASS_NAME, 'ItfyB')
            contact_name_tag_lst = msg_sub.find_elements(By.CLASS_NAME, 'a71At')
            if contact_name_tag_lst:
                return contact_name_tag_lst[0].text

            span = msg_sub.find_element(By.TAG_NAME, "span")
            aria_label = span.get_attribute("aria-label")
            if aria_label:
                return aria_label[:-1]

            copyable_text = msg_sub.find_elements(By.CLASS_NAME, "copyable-text")
            data_pre_plain_text = copyable_text[0].get_attribute("data-pre-plain-text") if copyable_text else None
            if data_pre_plain_text:
                sender = data_pre_plain_text[data_pre_plain_text.find("]") + 2:-2]
                return sender
        except:
            print("couldn't get msg sender")

    def get_msg_date(self, none_if_day_missing=True) -> dt.datetime:
        if (not self._msg_date) and none_if_day_missing:
            return None
        msg_time = self._get_msg_hour()
        msg_datetime = dt.datetime.combine(date=self._msg_date, time=msg_time)
        return msg_datetime

    def _get_msg_hour(self) -> dt.time:
        try:
            # sub options: gq1t1y46 lak21jic e4p1bexh cr2cog7z le5p0ye3 _1WSmF
            sub = self._obj.find_element(By.CLASS_NAME, '_1WSmF')  # lak21jic
            sub2 = sub.find_element(By.CLASS_NAME, 'fewfhwl7')

            # sub = self._obj.find_element(By.XPATH,
            #                              "//div[contains(@class,'lak21jic')]//span[contains(@class,'fewfhwl7')]")
            time_txt = sub2.text  # .find_element(By.TAG_NAME, 'span').text
            time_lst = time_txt.split(':')
            hour_str = int(time_lst[0])
            minutes_str = int(time_lst[1])
            mgs_time = dt.time(hour=hour_str, minute=minutes_str)
            return mgs_time
        except:
            print("couldn't execute time")

    def get_msg_receiving_status(self):
        if self.get_msg_sender() == 'את/ה':
            sub = self._obj.find_element(By.CLASS_NAME, '_1OX94')
            sub2 = sub.find_element(By.CLASS_NAME, '_2F01v')
            receiving_status = sub2.find_element(By.TAG_NAME, 'span').get_attribute('aria-label')
            return receiving_status
        else:
            raise Exception("receiving status is only available when you send the message")

    def set_msg_date(self, date: dt.date):
        self._msg_date = date

    def _check_class(self):
        print(self.get_msg_sender())
        print(self._get_msg_hour())
        if self.get_msg_sender() == 'את/ה:':
            print(self.get_msg_receiving_status())
