import datetime as dt
import time

import pandas

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import *

import sys

sys.path.append('../../whatsapp_objects')

from .generic_class import FatherWhatsappMsg


class DeletedMsg(FatherWhatsappMsg):
    def __init__(self, selenium_obj, msg_date: dt.date = None):
        super(DeletedMsg, self).__init__(selenium_obj, msg_date)
        self.msg_type = 'deleted'
        print('deleted msg')
