import datetime as dt
import re
import sys

import selenium.common.exceptions

sys.path.append('../../../../auto_first_dojh')

from utils import round_to_last_weekday


class BlueGeneral:
    def __init__(self, selenium_obj):
        self._is_valid = True
        self._obj = selenium_obj  # .find_element(by=By.CLASS_NAME, value='copyable-text')
        self._blue_raw_content = self._get_blue_text()
        self._classes = self._get_blue_classes()
        self.msg_type = 'blue'

    def get_date(self):
        if self._get_blue_type() == 'absolute date':
            return self._get_absolute_date()
        elif self._get_blue_type() == 'relative date':
            return self._get_relative_date()
        else:
            return None

    def create_blue_json(self):
        blue_json = {}
        blue_type = self._get_blue_type()
        blue_json['blue_type'] = blue_type
        if blue_type == 'absolute date':
            blue_json['date'] = self._get_absolute_date()
        if blue_type == 'relative date':
            blue_json['date'] = self._get_relative_date()
        return blue_json

    def _get_absolute_date(self):
        date_str = self._blue_raw_content
        date_lst = date_str.split('.')
        day = int(date_lst[0])
        month = int(date_lst[1])
        year = int(date_lst[2])
        blue_date = dt.date(year=year, month=month, day=day)
        return blue_date

    def _get_relative_date(self):
        now = dt.date.today()
        days_dict = {
            'ראשון': 'sunday',
            'שני': 'monday',
            'שלישי': 'tuesday',
            'רביעי': 'wednesday',
            'חמישי': 'thursday',
            'שישי': 'friday',
            'שבת': 'saturday'
        }
        if self._blue_raw_content == 'היום':
            return now
        if self._blue_raw_content == 'אתמול':
            return now - dt.timedelta(days=1)
        for hebrew_day in days_dict.keys():
            if self._blue_raw_content == hebrew_day:
                english_day = days_dict[hebrew_day]
                return round_to_last_weekday(now, weekday_to_round_to=english_day)

    def _get_blue_type(self):
        if not self._is_valid:
            return 'invalid blue'
        if self._blue_raw_content.startswith('קוד האבטחה עם'):
            return 'changed secure code'
        if 'עזב/ה' in self._blue_raw_content:
            return 'someone left group'
        if 'הצטרף/הבאמצעות קישור ההזמנה של קבוצה זו' in self._blue_raw_content:
            return 'someone join the group with the link'
        if 'נוסף על ידי' in self._blue_raw_content:
            return 'someone added someone to the group'
        if self._blue_raw_content.startswith('שינית את הנושא מ"'):
            return 'you changed the group subject'
        if 'שינה את הנושא מ"' in self._blue_raw_content:
            return 'someone changed the subject name'
        if self._blue_raw_content in ('היום', 'אתמול', 'ראשון', 'שני', 'שלישי', 'רביעי', 'חמישי', 'שישי', 'שבת'):
            return 'relative date'
        date_pattern = '[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{4}'
        if re.match(date_pattern, self._blue_raw_content):
            return 'absolute date'
        if 'g0rxnol2' in self._classes:
            return 'emoji reply'
        if self._blue_raw_content == 'הודעה 1 שלא נקראה' or self._blue_raw_content[-17:] == ' הודעות שלא נקראו':
            return 'unread msgs'
        else:
            print("couldn't find a match")
            return 'invalid blue'

    def _get_blue_text(self):
        try:
            date_selenium_obj = self._obj  # .find_element(By.TAG_NAME, 'div')  # 'i0jNr')
            blue_text: str = date_selenium_obj.text
            return blue_text.strip()
        except selenium.common.exceptions.StaleElementReferenceException:
            self._is_valid = False

    def _get_blue_classes(self):
        try:
            return self._obj.get_attribute('class').split()
        except selenium.common.exceptions.StaleElementReferenceException:
            self._is_valid = False
            return [None]
