import datetime as dt
import time

import pandas

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import *

import sys

sys.path.append('../../whatsapp_objects')

from .generic_class import FatherWhatsappMsg


class TextMsg(FatherWhatsappMsg):
    def __init__(self, selenium_obj, msg_date: dt.date = None):
        super(TextMsg, self).__init__(selenium_obj, msg_date)
        self.msg_type = 'text'

    def get_msg_content(self):
        sub_obj = self._obj.find_element(by=By.CLASS_NAME, value='_1Gy50')
        sub_obj2 = sub_obj.find_element(by=By.CLASS_NAME, value='i0jNr')
        sub_obj3 = sub_obj2.find_element(by=By.TAG_NAME, value='span')
        return sub_obj3.text  # TODO - add emojies
