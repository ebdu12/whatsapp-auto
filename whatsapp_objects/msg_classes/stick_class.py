import datetime as dt
import time

import pandas

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import *

import sys

sys.path.append('../../whatsapp_objects')

from .generic_class import FatherWhatsappMsg


class StickMsg(FatherWhatsappMsg):
    def __init__(self, selenium_obj, msg_date: dt.date = None):
        super(StickMsg, self).__init__(selenium_obj, msg_date)
        self.msg_type = 'stick'

    def get_msg_sender(self):
        try:
            sub = self._obj.find_element(By.CLASS_NAME, 'a71At')
            sender = sub.get_attribute('innerHTML')
            return sender
        except:
            self._obj.click()
            print('here')