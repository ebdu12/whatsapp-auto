from selenium.webdriver.common.by import By
import selenium.common.exceptions
import sys

from .deleted_class import DeletedMsg
from .img_class import ImgMsg
from .locations_class import LocationMsg
from .other_class import UnknownMsg
from .stick_class import StickMsg
from .text_class import TextMsg

sys.path.append('../../whatsapp_objects')

from .blue_ntfs.generic_blue import BlueGeneral


def create_msg_obj(selenium_obj):
    try:
        obj_classes = selenium_obj.get_attribute('class').split()
    except selenium.common.exceptions.StaleElementReferenceException as e:
        return None
    if 'focusable-list-item' not in obj_classes:
        print('You must put a selenium object with the class "focusable-list-item"!')
        return None
        raise Exception('class must be focusable-list-item')
    if "message-out" not in obj_classes and "message-in" not in obj_classes:
        return BlueGeneral(selenium_obj=selenium_obj)

    sub_selenium_obj = selenium_obj.find_element(by=By.XPATH, value='./div')
    sub_selenium_obj_class = sub_selenium_obj.get_attribute('class').split()
    locs_class = '_25eIs'
    text_class = '_3mSPV'
    img_class = '_1pC3e'
    youtube_class = '_2UizM'
    # todo add a file class
    # deleted_class = '_8YVHI'
    stick_class = '_1Tdp1'
    if text_class in sub_selenium_obj_class or youtube_class in sub_selenium_obj_class:
        return TextMsg(sub_selenium_obj)
    elif img_class in sub_selenium_obj_class:
        return ImgMsg(sub_selenium_obj)
    elif locs_class in sub_selenium_obj_class:
        return LocationMsg(sub_selenium_obj)
    elif stick_class in sub_selenium_obj_class:
        return StickMsg(sub_selenium_obj)
    # elif deleted_class in sub_selenium_obj_class:
    #     return DeletedMsg(sub_selenium_obj)
    else:
        return UnknownMsg(sub_selenium_obj)
