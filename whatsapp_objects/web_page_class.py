import time

from selenium import webdriver
from selenium.common.exceptions import *
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from config.selenium_conf import DRIVER_PATH


class WebPage:
    def __init__(self, url, browser="chrome", driver_path=DRIVER_PATH,
                 driver_options=None):
        if browser != "chrome":
            raise TypeError("class supports chrome browser only")

        if driver_options is None:
            self._driver = webdriver.Chrome(driver_path)
        else:
            self._driver = webdriver.Chrome(driver_path, options=driver_options)
        self._driver.get(url)

    def click_button(self, identify, identify_by=By.ID):
        link = self.find_element(identify_by=identify_by, identify=identify)
        link.click()

    def search_in_search_line(self, search_text, identify, identify_by=By.ID, to_click=True, is_slowly=False,
                              how_long_wait=10):
        search = self.find_element(identify_by=identify_by, identify=identify, how_long_wait=how_long_wait)
        search.clear()
        if is_slowly:
            self._send_keys_slowly(search_text, selenium_element=search)
        else:
            search.send_keys(search_text)

        if to_click:
            search.send_keys(Keys.RETURN)

    def go_backward(self):
        self._driver.back()

    def go_forward(self):
        self._driver.forward()

    def find_element(self, identify, identify_by=By.ID, how_long_wait=10):
        wait = WebDriverWait(self._driver,
                             how_long_wait,
                             poll_frequency=1,
                             ignored_exceptions=[ElementNotVisibleException, ElementNotSelectableException])
        elem = wait.until(EC.element_to_be_clickable((identify_by, identify)))
        return elem

    def find_elements(self, identify, identify_by=By.CLASS_NAME, how_long_wait=10):
        self._driver.implicitly_wait(how_long_wait)
        elem = self._driver.find_elements(by=identify_by, value=identify)
        return elem

    def close_page(self):
        self._driver.quit()

    def _send_keys_slowly(self, keys_to_send, selenium_element):
        actions = ActionChains(self._driver)
        for key in keys_to_send:
            time.sleep(3)
            actions.key_down(key, selenium_element)
            time.sleep(0.07)
            actions.key_up(key, selenium_element)

    def get_cookies(self):
        self._driver.get_cookies()

    def add_cookies(self, cookie):
        self._driver.add_cookie(cookie)

    def scroll(self, main_obj_value='body', main_obj_by=By.TAG_NAME, down=True, scroll_count=1, scrolls_hesitate=0):
        up_or_down_key = Keys.END if down else Keys.HOME

        try:
            obj_on_which_scroll = self.find_element(identify=main_obj_value, identify_by=main_obj_by)
        except Exception as e:
            time.sleep(3)
            print('excepted obj_on_which_scroll. I f there is a print of "fixed" then its ok')
            obj_on_which_scroll = self.find_element(identify=main_obj_value, identify_by=main_obj_by)
            print('fixed')

        for _ in range(scroll_count):
            time.sleep(scrolls_hesitate)
            obj_on_which_scroll.send_keys(Keys.CONTROL + up_or_down_key)
            obj_on_which_scroll.send_keys(Keys.CONTROL + up_or_down_key)

    def scroll_until(self, scroll_to_obj_value, scroll_to_obj_by, main_obj_value='body', main_obj_by=By.TAG_NAME,
                     down=True):
        pass
