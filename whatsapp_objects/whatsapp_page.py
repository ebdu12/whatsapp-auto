import datetime as dt
import time

import pandas

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import *

from config.selenium_conf import COOKIE, CHROME_USER, WHATSAPP_URL
from whatsapp_objects.msg_classes.blue_ntfs.generic_blue import BlueGeneral
from whatsapp_objects.web_page_class import WebPage
from whatsapp_objects.msg_classes.msg_generic_obj_creation import create_msg_obj


class WhatsApp(WebPage):
    def __init__(self):
        options = webdriver.ChromeOptions()
        options.add_argument(CHROME_USER)

        WebPage.__init__(self, url=WHATSAPP_URL, driver_options=options)

    def get_into_a_chat(self, whatsapp_group_name):
        self.search_in_search_line(whatsapp_group_name, identify='_13NKt',
                                   identify_by=By.CLASS_NAME, how_long_wait=60 * 5)
        time.sleep(5)  # let the chat some time to load

    def get_chat_messages(self, whatsapp_group_name=None):
        if whatsapp_group_name:
            self.get_into_a_chat(whatsapp_group_name=whatsapp_group_name)
        full_msg_selenium_obj = self.find_elements(identify='focusable-list-item', identify_by=By.CLASS_NAME)
        msg_list = []
        for i in full_msg_selenium_obj:
            msg = create_msg_obj(i)
            msg_list.append(msg) if msg else None

        return msg_list

    def get_all_chat_messages(self, whatsapp_group_name=None):
        self.get_into_a_chat(whatsapp_group_name=whatsapp_group_name) if whatsapp_group_name else None

        len_old = None
        len_new = 0
        while len_old != len_new:
            len_old = len_new
            self.scroll(main_obj_value='_33LGR', main_obj_by=By.CLASS_NAME, down=False, scroll_count=2,
                        scrolls_hesitate=5)
            len_new = self._find_shown_msgs_num()
            if len_old == len_new:
                print('oops. another try')
                self.scroll(main_obj_value='_33LGR', main_obj_by=By.CLASS_NAME, down=False, scroll_count=5,
                            scrolls_hesitate=10)
            len_new = self._find_shown_msgs_num()

        msg_list = self.get_chat_messages()
        return msg_list

    def get_chat_messages_by_date(self, min_date=None, whatsapp_group_name=None):
        self.get_into_a_chat(whatsapp_group_name=whatsapp_group_name) if whatsapp_group_name else None

        while True:
            self.scroll(main_obj_value='_33LGR', main_obj_by=By.CLASS_NAME, down=False, scroll_count=10,
                        scrolls_hesitate=5)
            msgs = self.get_chat_messages()  # todo make it differently, maybe "get all blue" or something
            blue_msgs = [msg for msg in msgs if isinstance(msg, BlueGeneral)]
            if any([msg.get_date() < min_date for msg in blue_msgs if msg.get_date()]):
                break
        return msgs

    def send_message(self, msg_content, chat_name: str = None):
        if chat_name is not None:
            self.get_into_a_chat(whatsapp_group_name=chat_name)
        self.search_in_search_line(msg_content, "//div[@title='הקלד/י הודעה']", By.XPATH)

    def _find_shown_msgs_num(self):
        try:
            msgs_len = len(self.find_elements('focusable-list-item', By.CLASS_NAME))
        except:
            time.sleep(5)
            try:
                msgs_len = len(self.find_elements('focusable-list-item', By.CLASS_NAME))
            except:
                print("couldn't find 'focusable-list-item' element for some reason")
                raise Exception("couldn't find 'focusable-list-item' element for some reason")
        return msgs_len
