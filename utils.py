import datetime as dt
import pickle


class PickleObj:
    def __init__(self, file_path):
        self._file_path = file_path

    def write_over_pkl(self, input_obj, run_over=True):
        if run_over is False:
            return Exception("Can't handle without run over yet")

        outfile = open(self._file_path, 'wb')
        pickle.dump(input_obj, outfile)

    def read_pkl(self):
        infile = open(self._file_path, 'rb')
        pkl_content = pickle.load(infile)
        return pkl_content


def round_to_last_weekday(date_input: dt.date, weekday_to_round_to='monday'):
    weekday_nums_dict = {
        'monday': 0,
        'tuesday': 1,
        'wednesday': 2,
        'thursday': 3,
        'friday': 4,
        'saturday': 5,
        'sunday': 6,
    }
    input_weekday = date_input.weekday()
    weekday_to_round_num = weekday_nums_dict[weekday_to_round_to]
    days_to_minus = input_weekday - weekday_to_round_num
    if days_to_minus <= 0:
        days_to_minus = days_to_minus + 7
    output = date_input - dt.timedelta(days=days_to_minus)
    return output
