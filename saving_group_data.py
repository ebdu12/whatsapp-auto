import pandas as pd


def create_msgs_df(msgs):
    df = pd.DataFrame(columns=['date', 'sender', 'type', 'content'])
    for msg in msgs:
        msg_type = msg.msg_type
        msg_data = {}
        if msg_type == 'blue':
            continue
        date = msg.get_msg_date()
        if not date:
            continue
        msg_data['date'] = date
        msg_data['sender'] = msg.get_msg_sender()
        msg_data['type'] = msg_type
        if msg_type == 'text':
            msg_data['content'] = msg.get_msg_content()
        else:
            msg_data['content'] = None

        df = df.append(msg_data, ignore_index=True)
    return df
