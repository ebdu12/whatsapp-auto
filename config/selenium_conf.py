DRIVER_PATH = "C:\Program Files (x86)\chromedriver.exe"

CHROME_USER = r'--user-data-dir=C:\Users\97252\AppData\Local\Google\Chrome\User Data'
WHATSAPP_URL = 'https://web.whatsapp.com/'

COOKIE = {
    # 'domain': '.web.whatsapp.com',
    'expiry': 1667562486,
    'httpOnly': True,
    'name': 'wa_beta_version',
    'path': '/',
    'sameSite': 'None',
    'secure': True,
    'value': 'production%2F1665444862%2F2.2238.7'
}
