import datetime as dt

from saving_group_data import create_msgs_df
from whatsapp_objects.msg_classes.blue_ntfs.generic_blue import BlueGeneral
from whatsapp_objects.whatsapp_page import WhatsApp

if __name__ == "__main__":
    WHATSAPP_GROUP_NAME = 'פוליטיקה עד המוות'

    whats_app = WhatsApp()

    min_date = dt.date(2022, 10, 1)
    msgs = whats_app.get_chat_messages_by_date(min_date, WHATSAPP_GROUP_NAME)
    current_date = None
    for msg in msgs:
        if isinstance(msg, BlueGeneral):
            current_date = msg.get_date()
            continue
        msg.set_msg_date(current_date) if current_date else None

    df = create_msgs_df(msgs)
    df.to_csv('group_msgs.csv')
    print('oops')
    whats_app.close_page()
